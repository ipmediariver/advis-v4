<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DateHistory extends Model
{
    
	protected $guarded = [];

	public function date(){

		return $this->belongsTo(Date::class, 'date_id');

	}


	public function getDateAttribute(){

		return $this->created_at->format('d M, Y h:i a');

	}

}
