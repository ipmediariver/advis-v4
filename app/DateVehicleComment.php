<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DateVehicleComment extends Model
{
    
	protected $guarded = [];


	public function vehicle(){

		return $this->belongsTo(DateVehicle::class, 'date_vehicle_id');

	}


	public function user(){

		return $this->belongsTo(User::class, 'user_id');

	}


	public function getDateAttribute(){

		return $this->created_at->format('d M, Y h:i a');

	}

	public function getAuthorAttribute(){

		return $this->user->full_name;

	}


}
