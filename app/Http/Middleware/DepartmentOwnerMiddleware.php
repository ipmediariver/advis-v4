<?php

namespace App\Http\Middleware;

use Closure;

class DepartmentOwnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $department = request('department');

        $user = auth()->user();

        $role = $user->role;

        if($role == 'admin' || $role == 'guard'){

            return $next($request);

        }elseif($role == 'host'){

            if($user->host->department_id == $department->id){

                return $next($request);

            }else{

                return redirect()
                    ->back()
                    ->with('warning', 'No tienes permisos para ingresar a este departamento.');

            }

        }

    }
}
