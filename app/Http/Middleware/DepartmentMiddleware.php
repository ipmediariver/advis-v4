<?php

namespace App\Http\Middleware;

use App\Department;
use Closure;

class DepartmentMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $find_department = Department::first();

        if($find_department){
        
            return $next($request);

        }else{

            return redirect()
                ->route('departments.create')
                ->with('warning', 'Por favor crea un departamento para continuar.');

        }

    }
}
