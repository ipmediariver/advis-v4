<?php

namespace App\Http\Middleware;

use Closure;

class DateOwnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $date = request('date');

        $user = auth()->user();

        $role = $user->role;

        if($role == 'admin' || $role == 'guard'){

            return $next($request);

        }elseif($role == 'host'){

            if($date->host_id == $user->host->id){
 
                return $next($request);

            }else{

                return redirect()
                    ->back()
                    ->with('warning', 'No puedes acceder a esta cita ya que esta asignada a otro encargado.');

            }

        }

    }
}
