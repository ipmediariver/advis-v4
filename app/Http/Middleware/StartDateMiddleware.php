<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class StartDateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $date = request('date');

        $start_at = $date->start_at->format('Y-m-d');
        $today    = Carbon::today()->format('Y-m-d');

        if ($start_at !== $today) {

            return redirect()
                ->back()
                ->withErrors([

                    'Esta cita no corresponde al día de hoy, si necesitas que el visitante ingrese, por favor crea una cita nueva.',

                ]);

        } else {

            return $next($request);

        }

    }
}
