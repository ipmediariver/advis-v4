<?php

namespace App\Http\Middleware;

use Closure;

class NotificationsOwnerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   

        $user = request('user');

        $current_user = auth()->user();

        if($user->id == $current_user->id){

            return $next($request);

        }else{

            return redirect()
                ->back()
                ->with('warning', 'No tienes permisos para ingresar a esta área.');

        }

    }
}
