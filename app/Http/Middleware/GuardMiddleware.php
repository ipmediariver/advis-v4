<?php

namespace App\Http\Middleware;

use Closure;

class GuardMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $user = auth()->user();

        $role = $user->role;

        if($role == 'guard'){

            return redirect()
                ->back()
                ->withErrors([
                    'No tienes permisos para ingresar a esta área'
                ]);

        }else{

            return $next($request);

        }

    }
}
