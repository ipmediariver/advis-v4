<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Requests\CompanyRequest;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(request()->ajax()){

            $companies = Company::where('id', '<>', 1)
                ->orderBy('name')
                ->get();

            return response()->json($companies);

        }else{

            $companies = Company::where('id', '<>', 1)
                ->orderBy('name')
                ->paginate(10);

            return view('app.companies.index', compact('companies'));

        }

    }


    public function dates(Company $company){

        $dates = $company->dates()->orderBy('start_at')->paginate(10);

        return view('app.companies.dates', compact('company', 'dates'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('app.companies.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {
        
        $company = Company::create([
            'name' => $request->name
        ]);

        if(request()->ajax()){

            return response()
                ->json($company);

        }else{

            return redirect()
                ->route('companies.index')
                ->with('msg', "Se ha creado la compañía {$company->name}");

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        
        return view('app.companies.show', compact('company'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        
        return view('app.companies.edit', compact('company'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, Company $company)
    {
        
        $company->update($request->all());

        return redirect()->route('companies.show', $company)->with('msg', "Se ha actualizado la información de la compañía.");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        
        $company->dates()->each(function($date){

            $date->visitors()->delete();

        });

        $company->dates()->delete();

        $company->visitors()->delete();

        $company->delete();

        return redirect()
            ->route('companies.index')
            ->with('warning', "La compañía ha sido eliminada.");

    }
}
