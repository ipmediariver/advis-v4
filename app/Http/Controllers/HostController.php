<?php

namespace App\Http\Controllers;

use App\Date;
use App\Department;
use App\Employee;
use App\Host;
use App\Http\Requests\HostRequest;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Http\Request;

class HostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Department $department)
    {

        if (request()->ajax()) {

            $hosts = $department->hosts;

            return response()
                ->json($hosts);

        } else {

            $hosts = $department->hosts()->paginate(10);

            return view('app.hosts.index', compact('department', 'hosts'));

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Department $department)
    {

        return view('app.hosts.create', compact('department'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HostRequest $request, Department $department)
    {

        if ($request->has('employee_id')) {

            $employee_is_assigned_to_department = $this->validate_if_employee_is_assigned_to_department($request, $department);

            if ($employee_is_assigned_to_department) {

                $already_assigned = $this->validate_if_employee_is_already_host_from_department($request, $department);

                if ($already_assigned) {

                    return redirect()
                        ->back()
                        ->with('info', 'El personal seleccionado ya se encuentra asignado como encargado en el departamento.');

                } else {

                    $employee = Employee::find($request->employee_id);

                    $user = $this->create_a_user_for_new_host($employee);

                    $host = $this->add_new_host_to_department($employee, $user, $department);

                    return redirect()
                        ->route('departments.hosts.index', $department)
                        ->with('msg', "Se ha asignado a {$host->full_name} para administrar el departamento {$department->name}");

                }

            }

        } else {

            $employee = $this->create_new_employee_to_set_a_new_host($request, $department);

            $user = $this->create_a_user_for_new_host($employee);

            $host = $this->add_new_host_to_department($employee, $user, $department);

            return redirect()
                ->route('departments.hosts.index', $department)
                ->with('msg', "Se ha asignado a {$host->full_name} para administrar el departamento {$department->name}");

        }

    }

    public function create_new_employee_to_set_a_new_host($request, $department)
    {

        $employee = Employee::create([
            'department_id' => $department->id,
            'first_name'    => $request->first_name,
            'last_name'     => $request->last_name,
            'email'         => $request->email,
            'jobtitle'      => $request->jobtitle,
            'phone'         => $request->phone,
            'ext'           => $request->ext,
            'mobile'        => $request->mobile,
            'gender_id'     => $request->gender_id,
            'employee_id'   => hexdec(uniqid()),
        ]);

        return $employee;

    }

    public function add_new_host_to_department($employee, $user, $department)
    {

        $host = Host::create([
            'department_id' => $department->id,
            'user_id'       => $user->id,
            'employee_id'   => $employee->id,
        ]);

        return $host;

    }

    public function create_a_user_for_new_host($employee)
    {

        $faker = \Faker\Factory::create();

        $password = $faker->password;

        $user = User::create([

            'first_name' => $employee->first_name,
            'last_name'  => $employee->last_name,
            'email'      => $employee->email,
            'role'       => 'host',
            'password'   => \Hash::make($password),

        ]);

        return $user;

    }

    public function validate_if_employee_is_assigned_to_department($request, $department)
    {

        $employee = $department->employees()->find($request->employee_id);

        return $employee ? true : false;

    }

    public function validate_if_employee_is_already_host_from_department($request, $department)
    {

        $host = $department->hosts()->where('employee_id', $request->employee_id)->first();

        return $host ? true : false;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Host  $host
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department, Host $host)
    {

        $dates = $host->dates()->paginate(10);

        return view('app.hosts.show', compact('department', 'host', 'dates'));

    }


    public function get_single_host(User $user){

        if($user->host){

            return response()->json($user->host);

        }

    }

    public function get_single_host_with_dates(User $user){

        if($user->host){

            $response = [

                'user' => $user,
                'host' => $user->host,
                'dates' => Date::format_dates_to_added_to_calendar($user->host->dates),

            ];

            return response()->json($response);

        }

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Host  $host
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department, Host $host)
    {

        return view('app.hosts.edit', compact('department', 'host'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Host  $host
     * @return \Illuminate\Http\Response
     */
    public function update(HostRequest $request, Department $department, Host $host)
    {

        $this->update_employee_data($request, $department, $host);

        $this->update_user_data($request, $department, $host);

        return redirect()
            ->route('departments.hosts.show', [$department, $host])
            ->with("msg", "La información del personal ha sido modificada exitosamente.");

    }

    public function update_employee_data($request, $department, $host)
    {

        $employee = $host->employee;

        $employee->update([

            'first_name' => $request->first_name,
            'last_name'  => $request->last_name,
            'email'      => $request->email,
            'jobtitle'   => $request->jobtitle,
            'phone'      => $request->phone,
            'ext'        => $request->ext,
            'mobile'     => $request->mobile,
            'gender_id'  => $request->gender_id,

        ]);

    }

    public function update_user_data($request, $department, $host)
    {

        $user = $host->user;

        $user->update([

            'first_name' => $request->first_name,
            'last_name'  => $request->last_name,
            'email'      => $request->email,

        ]);

        if ($request->password) {

            $user->update([

                'password' => \Hash::make($request->password),

            ]);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Host  $host
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department, Host $host)
    {
        
        if(count($host->dates)){

            $host->dates()->each(function($date){
                $date->visitors()->delete();
            });

            $host->dates()->delete();

        }

        $host->user->delete();

        $host->delete();

        return redirect()
            ->route('departments.hosts.index', $department)
            ->with('warning', "Se ha eliminado al encargado del departamento exitosamente.");

    }
}
