<?php

namespace App\Http\Controllers;

use App\Department;
use App\Employee;
use App\Http\Requests\EmployeeRequest;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{


    public function __construct(){

        // Es necesario crear un departamento para continuar

        $this->middleware('department')->only(['create', 'new_employee']);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Department $department)
    {

        $employees = $department->employees()->orderBy('first_name')->paginate(10);

        return view('app.employees.index', compact('department', 'employees'));

    }

    // Este metodo se utiliza cuando se crea un empleado fuera de un departamento.

    public function all(){

        $employees = Employee::orderBy('first_name')->paginate(10);

        return view('app.employees.all', compact('employees'));

    }

    // Este metodo se utiliza cuando se crea un empleado fuera de un departamento.

    public function new_employee(){

        $departments = Department::orderBy('name')->get();

        return view('app.employees.new', compact('departments'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Department $department)
    {

        $departments = Department::orderBy('name')->get();


        return view('app.employees.create', compact('department', 'departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request, Department $department)
    {

        $employee = Employee::create([

            'department_id' => $department->id,
            'first_name'    => $request->first_name,
            'last_name'     => $request->last_name,
            'email'         => $request->email,
            'jobtitle'      => $request->jobtitle,
            'phone'         => $request->phone,
            'ext'           => $request->ext,
            'mobile'        => $request->mobile,
            'gender_id'     => $request->gender_id,
            'employee_id'   => hexdec(uniqid()),
            'status_id'     => $request->status_id,

        ]);

        return redirect()
            ->route('departments.employees.show', [$department, $employee])
            ->with('msg', "Se ha creado el empleado exitosamente.");

    }

    // Este metodo se utiliza cuando se crea un empleado fuera de un departamento.

    public function store_employee(EmployeeRequest $request){


        $employee = Employee::create([

            'department_id' => $request->department_id,
            'first_name'    => $request->first_name,
            'last_name'     => $request->last_name,
            'email'         => $request->email,
            'jobtitle'      => $request->jobtitle,
            'phone'         => $request->phone,
            'ext'           => $request->ext,
            'mobile'        => $request->mobile,
            'gender_id'     => $request->gender_id,
            'employee_id'   => hexdec(uniqid()),
            'status_id'     => $request->status_id,

        ]);

        return redirect()
            ->route('show-employee', $employee)
            ->with('msg', "Se ha creado el empleado exitosamente.");


    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department, Employee $employee)
    {   

        $histories = $employee->histories()->paginate(10);

        return view('app.employees.show', compact('department', 'employee', 'histories'));

    }

    // Este metodo se utiliza cuando se crea un empleado fuera de un departamento.

    public function show_employee(Employee $employee){

        $histories = $employee->histories()->paginate(10);

        return view('app.employees.show', compact('employee', 'histories'));

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department, Employee $employee)
    {

        $departments = Department::orderBy('name')->get();

        return view('app.employees.edit', compact('departments', 'employee'));

    }

    public function edit_employee(Employee $employee){

        $departments = Department::orderBy('name')->get();

        return view('app.employees.edit', compact('employee', 'departments'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, Department $department, Employee $employee)
    {

        $this->if_employee_change_department_then_remove_current_host($request, $employee);

        $employee->update($request->all());

        $this->active_or_unactive_employee($request, $employee);

        $this->validate_if_employee_is_host_and_update($request, $employee);

        return redirect()
            ->route('departments.employees.show', [$department, $employee])
            ->with('msg', "Se ha actualizado la información exitosamente.");

    }


    public function update_employee(EmployeeRequest $request, Employee $employee){

        $this->if_employee_change_department_then_remove_current_host($request, $employee);

        $employee->update($request->all());

        $this->active_or_unactive_employee($request, $employee);

        $this->validate_if_employee_is_host_and_update($request, $employee);

        return redirect()
            ->route('show-employee', $employee)
            ->with('msg', "Se ha actualizado la información exitosamente.");

    }


    public function if_employee_change_department_then_remove_current_host($request, $employee){

        if($request->department_id !== $employee->department_id){

            if($employee->host){

                $employee->host->delete();

            }

        }

    }


    public function active_or_unactive_employee($request, $employee)
    {

        if ($request->has('active')) {

            $employee->update(['active' => 1]);

        } else {

            $employee->update(['active' => 0]);

        }

    }

    public function validate_if_employee_is_host_and_update($request, $employee)
    {

        if ($employee->host) {

            $employee->host->user->update([

                'first_name' => $request->first_name,
                'last_name'  => $request->last_name,
                'email'      => $request->email,

            ]);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department, Employee $employee)
    {

        if ($employee->host) {

            $host = $employee->host;

            $host->user->delete();

            $host->delete();

        }

        $employee->histories()->delete();

        $employee->delete();

        return redirect()
            ->route('departments.employees.index', $department)
            ->with('warning', "El empleado ha sido eliminado.");

    }

    public function destroy_employee(Employee $employee){

       if ($employee->host) {

           $host = $employee->host;

           $host->user->delete();

           $host->delete();

       }

       $employee->histories()->delete();

       $employee->delete();

       return redirect()
           ->route('employees')
           ->with('warning', "El empleado ha sido eliminado."); 

    }

}
