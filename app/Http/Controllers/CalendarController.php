<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CalendarController extends Controller
{
    
	public function index(){

		$user = auth()->user();

		return view('app.calendar.index', compact('user'));

	}

}
