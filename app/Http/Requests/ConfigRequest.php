<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConfigRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'name'    => 'required|string',
            'address' => 'required|string',
            'phone'   => 'string|nullable',
            'rfc'     => 'string|nullable',

        ];
    }

    public function messages()
    {

        return [

            'name.required'    => 'El nombre de la compañía es requerido',
            'address.required' => 'La dirección de la compañía es requerida',
            'phone.string'     => 'El teléfono no tiene un formato válido',
            'rfc.string'       => 'El RFC no tiene un formato válido',

        ];

    }

}
