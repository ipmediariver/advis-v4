<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContainerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'name'        => 'required|string',
            'description' => 'string|nullable',
            'issuing_by'  => 'string|nullable',
            'padlock'     => 'string|nullable',
            'number'      => 'string|nullable',
            'petition'    => 'string|nullable',
            'comments'    => 'string|nullable',

        ];
    }

    public function messages()
    {

        return [

            'name.required' => 'El nombre del contenedor es requerido',

        ];

    }

}
