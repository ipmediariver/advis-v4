<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $company = request('company');

        return [
            'name' => ['required', Rule::unique('companies')->ignore($company ? $company->id : '')]
        ];
    }

    public function messages()
    {

        return [

            'name.required' => 'El nombre de la compañía es requerido',
            'name.unique'   => 'El nombre de la compañía no esta disponible',

        ];

    }

}
