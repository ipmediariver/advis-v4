<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VehicleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        $vehicle = request('vehicle');

        return [
            'company_id' => 'exists:companies,id|nullable',
            'type_id'    => 'required|in:0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15',
            'brand'      => 'required|string',
            'model'      => 'required|string',
            'plates'     => ['required','string','alpha_num',Rule::unique('vehicles')->ignore($vehicle ? $vehicle->id : '')],
            'color'      => 'required|string',
            'year'       => 'required|numeric',
        ];
    }

    public function messages()
    {

        return [

            'company_id.exists' => 'La compañía proporcionada no existe',
            'type_id.required'  => 'El tipo de vehículo es requerido',
            'type_id.in'        => 'El tipo de vehículo proporcionado no existe',
            'brand.required'    => 'La marca del vehículo es requerida',
            'model.required'    => 'El modelo del vehículo es requerido',
            'plates.required'   => 'Las placas del vehículo son requeridas',
            'plates.unique'     => 'Las placas proporcionadas ya se encuentran registradas',
            'plates.alpha_num'  => 'Las placas solo pueden contener números y letras',
            'color.required'    => 'El color del vehículo es requerido',
            'year.required'     => 'El año del vehículo es requerido',
            'year.numeric'      => 'El año del vehículo debe ser un número'

        ];

    }

}
