<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VisitorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'          => 'required|string',
            'last_name'           => 'required|string',
            'email'               => 'email|nullable',
            'jobtitle'            => 'string|nullable',
            'type_id'             => 'required|in:0,1,2,3',
            'gender_id'           => 'required|numeric|in:0,1',
            'company_id'          => 'exists:companies,id',
            'independent_visitor' => 'in:1',
        ];
    }

    public function messages()
    {

        return [

            'first_name.required'     => 'El nombre del visitante es requerido',
            'last_name.required'      => 'El apellido del visitante es requerido',
            'email.email'             => 'El correo electrónico proporcionado no es válido',
            'jobtitle.string'         => 'El puesto laboral proporcionado no es válido',
            'type_id.required'        => 'El tipo de visita es requerido',
            'type_id.in'              => 'El tipo de visita proporcionado no es válido',
            'gender_id.required'      => 'El género es requerido',
            'gender_id.numeric'       => 'El género no es válido',
            'gender_id.in'            => 'El género no es válido',
            'company_id.exists'       => 'La compañía seleccionada no es válida',
            'independent_visitor.in'  => 'Parece que hay un error, por favor vuelve a intentarlo.',

        ];

    }

}
