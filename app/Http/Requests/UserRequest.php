<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $request = request();

        $user = request('user');

        return [

            'first_name' => 'required|string',
            'last_name'  => 'required|string',
            'email'      => ['required', 'email', Rule::unique('users')->ignore($user ? $user->id : '')],
            'role'       => 'required|in:admin,terminal,guard',
            'password'   => 'min:6|max:15|confirmed|nullable',

        ];
    }

    public function messages()
    {

        return [

            'first_name.required' => 'El nombre del usuario es requerido',
            'last_name.required'  => 'El apellido del usuario es requerido',
            'email.required'      => 'El correo electrónico es requerido',
            'email.email'         => 'El correo electrónico no es válido',
            'email.unique'        => 'El correo electrónico no esta disponible',
            'role.required'       => 'El role es requerido',
            'role.in'             => 'El role proporcionado no es válido',
            'password.min'        => 'La contraseña debe contener al menos 6 caracteres',
            'password.max'        => 'La contraseña debe contener menos de 15 caracteres',
            'password.confirmed'  => 'La confirmación de la contraseña no coincide',

        ];

    }

}
