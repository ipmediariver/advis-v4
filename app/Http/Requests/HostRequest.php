<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class HostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $host = request('host');

        $request = request();

        return [

            'first_name'  => [
                'string',
                Rule::requiredIf(function () use ($request) {
                    return $request->has('employee_id') ? false : true;
                }),
            ],
            'last_name'   => [
                'string',
                Rule::requiredIf(function () use ($request) {
                    return $request->has('employee_id') ? false : true;
                }),
            ],
            'email'       => [
                'email',
                Rule::unique('employees')->ignore($host ? $host->employee->id : ''),
                Rule::unique('users')->ignore($host ? $host->user->id : ''),
                Rule::requiredIf(function () use ($request) {
                    return $request->has('employee_id') ? false : true;
                }),
            ],
            'gender_id'   => [
                'in:0,1',
                Rule::requiredIf(function () use ($request) {
                    return $request->has('employee_id') ? false : true;
                }),
            ],
            'jobtitle'    => [
                'string',
                Rule::requiredIf(function () use ($request) {
                    return $request->has('employee_id') ? false : true;
                }),
            ],
            'employee_id' => 'nullable|exists:employees,id',
            'password'    => 'min:6|max:15|confirmed|nullable',

        ];
    }

    public function messages()
    {

        return [

            'email.email'        => 'El correo electrónico no es válido',
            'email.unique'       => 'El correo electrónico no esta disponible',
            'gender_id.in'       => 'El género no es válido',
            'jobtitle.required'  => 'El puesto laboral es requerido',
            'jobtitle.string'    => 'El puesto laboral no es válido',
            'employee_id.exists' => 'El empleado proporcionado no es válido',
            'password.min'       => 'La contraseña debe contener al menos 6 caracteres',
            'password.max'       => 'La contraseña debe contener máximo 15 caracteres',
            'password.confirmed' => 'La contraseña no coincide con la confirmación',

        ];

    }

}
