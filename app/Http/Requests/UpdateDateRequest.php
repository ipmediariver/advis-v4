<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'subject'     => 'required|string',
            'description' => 'string|nullable',
            'start_at'    => 'required|date|date_format:d-m-Y h:i a',
            'vigency'     => 'required|in:1,2,5,8,0',

        ];
    }

    public function messages()
    {

        return [

            'subject.required'     => 'El asunto de la cita es requerido',
            'subject.string'       => 'El asunto de la cita no es válido',
            'description.string'   => 'La descripción de la cita no es valida',
            'start_at.required'    => 'La fecha de la cita es requerida',
            'start_at.date'        => 'La fecha de la cita no es válida',
            'start_at.date_format' => 'La fecha de la cita no tiene un formato válido',
            'vigency.required'     => 'La vigencia es requerida',
            'vigency.in'           => 'La vigencia no es válida',

        ];

    }
}
