<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeHistory extends Model
{
    
	protected $guarded = [];

	protected $hidden = [];

	public function getDateAttribute(){

		return $this->created_at->format('d M, Y h:i a');

	}

}
