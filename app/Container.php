<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Container extends Model
{
    
	protected $guarded = [];

	protected $hidden = [];

	protected $appends = [
		
	];


	public function date(){

		return $this->belongsTo(DateContainer::class, 'id', 'container_id');

	}


}
