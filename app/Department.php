<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{

    protected $guarded = [];

    protected $hidden = [];

    protected $appends = [
        'hosts_total',
        'dates_total',
        'employees_total',
    ];

    public function hosts()
    {

        return $this->hasMany(Host::class, 'department_id');

    }

    public function getHostsTotalAttribute()
    {

        return '(' . $this->hosts()->count() . ') Encargados';

    }

    public function employees()
    {

        return $this->hasMany(Employee::class, 'department_id')->orderBy('first_name');

    }

    public function getEmployeesTotalAttribute()
    {

        return '(' . $this->employees()->count() . ') Empleados';

    }

    public function dates()
    {

        return $this->hasMany(Date::class, 'department_id');

    }

    public function getDatesTotalAttribute()
    {

        return '(' . $this->dates()->count() . ') Citas';

    }

}
