<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DateContainer extends Model
{


	protected $guarded = [];


	protected $appends = [

		'name', 'issuing_by', 'padlock', 'number', 'company_name'

	];

    
	public function date(){

		return $this->belongsTo(Date::class);

	}

	public function container(){

		return $this->belongsTo(Container::class);

	}

	public function getNameAttribute(){

		return $this->container->name;

	}


	public function getIssuingByAttribute(){

		return $this->container->issuing_by;

	}


	public function getPadlockAttribute(){

		return $this->container->padlock;

	}


	public function getNumberAttribute(){

		return $this->container->number;

	}


	public function getCompanyNameAttribute(){

		return $this->date->company_name;

	}


}
