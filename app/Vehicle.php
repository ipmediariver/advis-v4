<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    

	protected $guarded = [];

	protected $hidden = [];

	protected $appends = [
		'name'
	];

	public function company(){

		return $this->belongsTo(Company::class, 'company_id');

	}

	public function getCompanyNameAttribute(){

		return $this->company->name;

	}

	public function dates(){

		return $this->hasMany(DateVehicle::class, 'vehicle_id');

	}

	public function getDatesTotalAttribute(){

		return '('.$this->dates()->count().') Citas';

	}

	public function getTypeAttribute(){

		switch ($this->type_id) {
			case 0:
				return 'MICRO';
				break;

			case 1:
				return 'SEDAN';
				break;

			case 2:
				return 'CUV';
				break;

			case 3:
				return 'SUV';
				break;

			case 4:
				return 'HATCHBACK';
				break;
			
			case 5:
				return 'ROADSTER';
				break;

			case 6:
				return 'PICKUP';
				break;

			case 7:
				return 'VAN';
				break;

			case 8:
				return 'COUPE';
				break;

			case 9:
				return 'SUPER-CAR';
				break;

			case 10:
				return 'CAMPER-VAN';
				break;

			case 11:
				return 'MINI-TRUCK';
				break;

			case 12:
				return 'CABRIOLET';
				break;

			case 13:
				return 'MINI-VAN';
				break;

			case 14:
				return 'TRUCK';
				break;

			case 15:
				return 'BIG-TRUCK';
				break;
		}

	}


	public function getNameAttribute(){

		
		return "{$this->brand} - {$this->model} | Año: {$this->year} | Color: {$this->color} | Placas: {$this->plates}";


	}

	public function getShortNameAttribute(){

		return "{$this->brand} {$this->model}";

	}

}
