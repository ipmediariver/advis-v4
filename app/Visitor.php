<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    
	protected $guarded = [];

	protected $hidden = [];

	protected $appends = [
		'full_name',
		'type'
	];


	public function dates(){

		return $this->hasMany(DateVisitor::class, 'visitor_id');

	}

	public function getDatesTotalAttribute(){

		return '('.$this->dates()->count().') Citas';

	}

	public function company(){

		return $this->belongsTo(Company::class, 'company_id');

	}

	public function getCompanyNameAttribute(){

		return $this->company->name;

	}

	public function getFullNameAttribute(){

		return $this->first_name .' '. $this->last_name;

	}

	public function getGenderAttribute(){

		switch ($this->gender_id) {
			case 0:
				return 'Masculino';
				break;
			
			case 1:
				return 'Femenino';
				break;
		}

	}


	public function getTypeAttribute(){

		switch ($this->type_id) {
			case 0:
				return 'Visita general';
				break;
			
			case 1:
				return 'Vendedor';
				break;

			case 2:
				return 'Proveedor';
				break;

			case 3:
				return 'Otro';
				break;
		}

	}


}
