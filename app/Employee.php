<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    
	protected $guarded = [];

	protected $hidden = [];

	protected $appends = [];


	public function host(){

		return $this->hasOne(Host::class, 'employee_id');

	}


	public function department(){

		return $this->belongsTo(Department::class, 'department_id');

	}

	public function getFullNameAttribute(){

		return $this->first_name .' '. $this->last_name;

	}

	public function getGenderAttribute(){

		switch ($this->gender_id) {
			case 0:
				return 'Masculino';
				break;
			
			case 1:
				return 'Femenino';
				break;
		}

	}

	public function getIsActiveAttribute(){

		if($this->active){

			return 'Empleado activo';

		}else{

			return 'Empleado inactivo';

		}

	}

	public function histories(){

		return $this->hasMany(EmployeeHistory::class, 'employee_id');

	}


	public function getStatusAttribute(){

		switch ($this->status_id) {
			case 0:
				return 'Operando';
				break;

			case 1:
				return 'En periodo de prueba';
				break;

			case 2:
				return 'Despedido';
				break;

			case 3:
				return 'De baja';
				break;

			case 4:
				return 'De vacaciones';
				break;

			case 5:
				return 'En incapacidad';
				break;

			case 6:
				return 'Practicante';
				break;
			
		}

	}

}
