<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Host extends Model
{
    
	protected $guarded = [];

	protected $hidden = [];

	protected $appends = [
		'full_name'
	];

	public function dates(){

		return $this->hasMany(Date::class, 'host_id');

	}

	public function getDatesTotalAttribute(){

		return '('.$this->dates()->count().') Citas';

	}

	public function user(){

		return $this->belongsTo(User::class, 'user_id');

	}

	public function getFullNameAttribute(){

		return $this->user->full_name;

	}

	public function getEmailAttribute(){

		return $this->user->email;

	}

	public function getJobtitleAttribute(){

		return $this->employee ? $this->employee->jobtitle : 'Sin puesto laboral';

	}

	public function employee(){

		return $this->belongsTo(Employee::class, 'employee_id');

	}

	public function getGenderAttribute(){

		return $this->employee->gender;

	}

	public function department(){

		return $this->belongsTo(Department::class, 'department_id');

	}

	public function getDepartmentNameAttribute(){

		return $this->department->name;

	}

}
