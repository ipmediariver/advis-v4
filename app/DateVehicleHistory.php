<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DateVehicleHistory extends Model
{
    
	protected $guarded = [];

	protected $hidden = [];

	public function vehicle(){

		return $this->belongsTo(DateVehicle::class, 'date_vehicle_id');

	}

}
