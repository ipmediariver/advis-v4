@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-sm-4 mx-auto text-center">
				<img src="{{ asset('img/undraw_Taken_if77.svg') }}" alt="{{ config('app.name') }}" class="img-fluid mb-4">
				<h1>Error 404</h1>
				<P class="lead">La página que intentas buscar no existe.</P>
				<p>Por favor regresa al inicio haciendo <a href="{{ route('home') }}">Click aquí</a></p>
			</div>
		</div>
	</div>

@stop