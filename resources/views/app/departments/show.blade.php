@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">{{ $department->name }} | Información general</p>
				<a href="{{ route('departments.edit', $department) }}" class="btn btn-primary ml-auto">
					<i class="fa fa-pencil-alt mr-2"></i>
					Editar
				</a>
			</div>
			<table class="table table-striped m-0">
				<tbody>
					<tr>
						<th width="20%">Nombre:</th>
						<td>{{ $department->name }}</td>
					</tr>
					<tr>
						<th>Encargados:</th>
						<td>
							<a href="{{ route('departments.hosts.index', $department) }}">
								{{ $department->hosts_total }}
							</a>
						</td>
					</tr>
					<tr>
						<th>Empleados:</th>
						<td>
							<a href="{{ route('departments.employees.index', $department) }}">
								{{ $department->employees_total }}
							</a>
						</td>
					</tr>
					<tr>
						<th>Citas:</th>
						<td>
							<a href="{{ route('department-dates', $department) }}">
								{{ $department->dates_total }}	
							</a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

@stop