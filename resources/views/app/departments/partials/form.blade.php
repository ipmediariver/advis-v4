@csrf	

<div class="form-group row">
	<div class="col-sm-6 offset-3">
		<p>Campos requeridos marcados con (<span class="text-danger">*</span>)</p>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Nombre:</label>
	<div class="col-sm-6">
		<input type="text" 
			name="name" 
			value="{{ isset($department) ? $department->name : old('name') }}" 
			class="form-control @error('name') is-invalid @enderror"
			required
			autofocus>
	</div>
</div>