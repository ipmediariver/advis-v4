@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Listado de departamentos</p>
				<a href="{{ route('departments.create') }}" class="btn btn-primary ml-auto">
					<i class="fa fa-plus mr-2"></i>
					Nuevo
				</a>
			</div>
			
			@if(count($departments))
			<table class="table table-striped m-0">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Encargados</th>
						<th>Empleados</th>
						<th>Citas</th>
						<th class="text-right">Detalles</th>
					</tr>
				</thead>

				<tbody>
					@foreach($departments as $department)
					<tr>
						<td>{{ $department->name }}</td>
						<td>
							<a href="{{ route('departments.hosts.index', $department) }}">
								{{ $department->hosts_total }}
							</a>
						</td>
						<td>
							<a href="{{ route('departments.employees.index', $department) }}">
								{{ $department->employees_total }}
							</a>
						</td>
						<td>
							<a href="{{ route('department-dates', $department) }}">
								{{ $department->dates_total }}
							</a>
						</td>
						<td class="text-right">
							<a href="{{ route('departments.show', $department) }}">
								Ver más
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			@else
			<div class="card-body">
				<div class="row">
					<div class="col-sm-4 mx-auto text-center">
						<img src="{{ asset('img/undraw_apartment_rent_o0ut.svg') }}" class="svg_img w-50 mb-4" alt="">
						<p class="lead">No se han agregado departamentos aún.</p>
						<a href="{{ route('departments.create') }}" class="btn btn-primary">
							<i class="fa fa-plus mr-2"></i> 
							Agrega uno
						</a>
					</div>
				</div>
			</div>
			@endif
	
			@if($departments->total() > $departments->perPage())
			<div class="card-footer">
				{{ $departments->links() }}
			</div>
			@endif
		</div>

	</div>

@stop