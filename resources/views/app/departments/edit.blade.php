@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Editar departamento</p>
			</div>
			<div class="card-body">
				
				<form action="{{ route('departments.update', $department) }}" method="post" id="updateDepartmentForm">
					
					@method('PATCH')

					@include('app.departments.partials.form')

				</form>

			</div>
			<div class="card-footer">
				<div class="row">
					<div class="col-sm-6 offset-3 d-flex align-items-center">
						<button type="submit" class="btn btn-primary" form="updateDepartmentForm">
							<i class="fa fa-check mr-2"></i>
							Actualizar
						</button>

						<form action="{{ route('departments.destroy', $department) }}" class="ml-2" method="post">
							@csrf
							@method('DELETE')
							<button type="submit" 
								class="btn btn-outline-danger"
								onclick="return confirm('¿Estás seguro que deseas eliminar este departamento?, al hacerlo se eliminarán todas las citas relacionadas.')">
								<i class="fa fa-trash-alt mr-2"></i>
								Eliminar
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>

	</div>

@stop