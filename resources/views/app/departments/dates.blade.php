@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		
		@component('app.components.dates-table', [
			
			'dates' => $dates,
			'title' => $department->name . ' | Listado de citas'	
	
		])

		@endcomponent

	</div>

@stop