@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Notificaciones</p>
			</div>

			<div class="list-group list-group-flush">
				@foreach($notifications as $notification)
				<li class="list-group-item">
					
					@if($notification->type == 'App\Notifications\NotifyToHostWhenVisitorIsArriving')

						{{ $notification->data['msg'] }}

					@else

					@endif

				</li>
				@endforeach
			</div>
		</div>

	</div>

@stop