@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Listado de usuarios</p>
				<a href="{{ route('users.create') }}" class="btn btn-primary ml-auto">
					<i class="fa fa-plus mr-2"></i>
					Nuevo
				</a>
			</div>
			
			@if(count($users))
				
			<table class="table table-striped m-0">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Correo electrónico</th>
						<th>Role</th>
						<th class="text-right">Detalles</th>
					</tr>
				</thead>

				<tbody>
					@foreach($users as $user)
					<tr>
						<td>
							<a href="{{ route('users.show', $user) }}">
								{{ $user->full_name }}
							</a>
						</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->role_name }}</td>
						<td class="text-right">
							<a href="{{ route('users.show', $user) }}">
								Ver más
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			@else

			<div class="card-body">
				<div class="row">
					<div class="col-sm-4 mx-auto text-center">
						<img src="{{ asset('img/undraw_programmer_imem.svg') }}" class="svg_img w-50 mb-4" alt="">
						<p class="lead">No se han agregado usuarios aún.</p>
						<a href="{{ route('users.create') }}" class="btn btn-primary">
							<i class="fa fa-plus mr-2"></i> 
							Agrega uno
						</a>
					</div>
				</div>
			</div>

			@endif
			
			@if($users->total() > $users->perPage())
			<div class="card-footer">
				{{ $users->links() }}
			</div>
			@endif
		</div>
	</div>

@stop