@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">{{ $department->name }} | Nuevo encargado de departamento</p>
			</div>

			@if($department->employees()->count())

			<div class="card-body border-bottom bg-light">
				
				<div class="form-group row">
					<div class="col-sm-6 offset-3">

						<p class="mb-3">Selecciona a uno de los empleados asignados a este departamento</p>
						
						<form action="{{ route('departments.hosts.store', $department) }}" method="post" class="d-flex align-items-center">
							
							@csrf

							<select name="employee_id" id="" class="custom-select @error('employee_id') is-invalid @enderror">
								
								<option disabled selected value="">Elige una opción</option>

								@foreach($department->employees as $employee)
								
								<option value="{{ $employee->id }}">{{ $employee->full_name }}</option>

								@endforeach

							</select>

							<button type="submit" 
								class="btn btn-primary ml-2 nowrap">
								
								<i class="fa fa-check mr-2"></i>
								
								Establecer como encargado

							</button>

						</form>

					</div>
				</div>

			</div>

			@endif

			<div class="card-body">
				
				<form action="{{ route('departments.hosts.store', $department) }}" id="newHostForm" method="post">

					@if($department->employees()->count())

					<div class="form-group row mb-0">
						<div class="col-sm-6 offset-3">
							<p class="lead">Ó bien crea uno nuevo</p>
						</div>
					</div>

					@endif

					@include('app.hosts.partials.form')

				</form>

			</div>

			<div class="card-footer pb-0">
				<div class="form-group row">
					<div class="col-sm-6 offset-3">
						<button type="submit" class="btn btn-primary" form="newHostForm">
							<i class="fa fa-check mr-2"></i>
							Guardar
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop