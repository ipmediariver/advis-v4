@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Editar información</p>
			</div>
			<div class="card-body">
				
				<form action="{{ route('departments.hosts.update', [$department, $host]) }}" method="post" id="updateHostForm">
					
					@method('PATCH')

					@include('app.hosts.partials.form')

				</form>

			</div>
			<div class="card-footer pb-0">
				<div class="form-group row">
					<div class="col-sm-6 offset-3 d-flex align-items-center">
						<button type="submit" class="btn btn-primary" form="updateHostForm">
							<i class="fa fa-check mr-2"></i>
							Actualizar
						</button>
				
						<form action="{{ route('departments.hosts.destroy', [$department, $host]) }}" method="post" class="ml-2">
							@csrf
							@method('DELETE')
							<button class="btn btn-outline-danger"
								onclick="return confirm('¿Estás seguro que deseas eliminar al encargado del departamento?, al hacerlo se eliminarán sus citas relacionadas.')">
								<i class="fa fa-trash-alt mr-2"></i>
								Eliminar
							</button>
						</form>

					</div>
				</div>
			</div>
		</div>
	</div>

@stop