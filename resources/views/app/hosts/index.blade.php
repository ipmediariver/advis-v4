@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">{{ $department->name }} | Encargados</p>
				<a href="{{ route('departments.hosts.create', $department) }}" class="btn btn-primary ml-auto">
					<i class="fa fa-plus mr-2"></i>
					Nuevo
				</a>
			</div>
			
			@if(count($hosts))
				
			<table class="table table-striped m-0">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Correo electrónico</th>
						<th>Puesto laboral</th>
						<th>Total de citas</th>
						<th class="text-right">Detalles</th>
					</tr>
				</thead>

				<tbody>
					@foreach($hosts as $host)

					<tr>
						<td>
							<a href="{{ route('departments.hosts.show', [$department, $host]) }}">
								{{ $host->full_name }}
							</a>
						</td>
						<td>{{ $host->email }}</td>
						<td>{{ $host->jobtitle }}</td>
						<td>{{ $host->dates_total }}</td>
						<td class="text-right">
							<a href="{{ route('departments.hosts.show', [$department, $host]) }}">
								Ver más
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			@else

			<div class="card-body">
				<div class="row">
					<div class="col-sm-4 mx-auto text-center">
						<img src="{{ asset('img/undraw_business_deal_cpi9.svg') }}" class="svg_img w-50 mb-4" alt="">
						<p class="lead">No se han agregado encargados en este departamento aún.</p>
						<a href="{{ route('departments.hosts.create', $department) }}" class="btn btn-primary">
							<i class="fa fa-plus mr-2"></i> 
							Agrega uno
						</a>
					</div>
				</div>
			</div>
	
			@endif

			@if($hosts->total() > $hosts->perPage())
	
			<div class="card-footer">
				{{ $hosts->links() }}
			</div>

			@endif
		</div>

	</div>

@stop