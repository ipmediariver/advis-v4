@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		
		@component('app.components.dates-table', [
			
			'title' => $company->name . ' | Citas',
			'dates' => $dates,
			'route' => route('dates.create') . '?company_id=' . $company->id
		
		])

		@endcomponent

	</div>

@stop