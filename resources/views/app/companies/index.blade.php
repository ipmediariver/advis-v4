@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Listado de compañías</p>
				<a href="{{ route('companies.create') }}" class="btn btn-primary ml-auto">
					<i class="fa fa-plus mr-2"></i>
					Nueva
				</a>
			</div>
			
			@if($companies->count())

			<table class="table table-striped m-0">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Total de visitantes</th>
						<th>Total de citas</th>
						<th>Total de vehículos</th>
						<th class="text-right">Detalles</th>
					</tr>
				</thead>

				<tbody>
					@foreach($companies as $company)
			
					<tr>
						<td>
							<a href="{{ route('companies.show', $company) }}">
								{{ $company->name }}
							</a>
						</td>
						<td>
							<a href="{{ route('companies.visitors.index', $company) }}">
								{{ $company->visitors_total }}
							</a>
						</td>
						<td>
							<a href="{{ route('company-dates', $company) }}">
								{{ $company->dates_total }}
							</a>
						</td>
						<td>
							<a href="{{ route('companies.vehicles.index', $company) }}">
								{{ $company->vehicles_total }}
							</a>
						</td>
						<td class="text-right">
							<a href="{{ route('companies.show', $company) }}">
								Ver más
							</a>
						</td>
					</tr>

					@endforeach
				</tbody>
			</table>

			@else
	
			<div class="card-body">
				<div class="row">
					<div class="col-sm-4 mx-auto text-center">
						<img src="{{ asset('img/undraw_Building_vpxo.svg') }}" class="svg_img w-50 mb-4" alt="">
						<p class="lead">No se han agregado compañías aún.</p>
						<a href="{{ route('companies.create') }}" class="btn btn-primary">
							<i class="fa fa-plus mr-2"></i> 
							Agrega una
						</a>
					</div>
				</div>
			</div>

			@endif
			
			@if($companies->total() > $companies->perPage())
			<div class="card-footer">
				{{ $companies->links() }}
			</div>
			@endif
		</div>

	</div>

@stop