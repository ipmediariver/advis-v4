@csrf

<div class="form-group row">
	<div class="col-sm-6 offset-3">
		<p>Campos requeridos marcados con (<span class="text-danger">*</span>)</p>
	</div>
</div>

<div class="form-group row mb-0">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Nombre:</label>
	<div class="col-sm-6">
		<input type="text" 
			name="name" 
			class="form-control @error('name') is-invalid @enderror"
			value="{{ isset($company) ? $company->name : old('name') }}"
			autofocus
			required>
	</div>
</div>