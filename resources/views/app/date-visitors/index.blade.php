@extends('layouts.app')

@section('content')

	<div class="container-fluid">

		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Listado de visitantes</p>
			</div>

			
			<div class="card-body border-bottom bg-light">
				
				<div class="row">
					<div class="col-sm-7 mx-auto">
						<div class="form-group m-0">
							
							@component('app.components.back-btn', [
								'route' => route('dates.show', $date),
								'text' => 'Regresar a la cita'
							])

							@endcomponent

							<p class="mb-3">
								Visitantes registrados en la compañía asociada a la cita.
							</p>


							<div class="d-flex align-items-center">
								
								<form action="{{ route('dates.visitors.store', $date) }}"
									class="d-flex align-items-center w-100 mr-2"
									method="post">

									@csrf
									
									<select name="visitor_id" id="" class="custom-select" required>
										
										<option value="">Elige un visitante</option>

										@foreach($date->company->visitors as $visitor)

										<option value="{{ $visitor->id }}">{{ $visitor->full_name }}</option>

										@endforeach

									</select>

									<button class="btn btn-outline-primary ml-2 nowrap">
										<i class="fa fa-check mr-2"></i>
										Agregar
									</button>

								</form>

								<div class="ml-auto pl-2 border-left">
									
									<button class="btn btn-primary nowrap" 
										data-toggle="modal"
										data-target="#newVisitorModal">
											
											<i class="fa fa-plus mr-2"></i>

											Crear nuevo visitante

									</button>

								</div>
							</div>

						</div>
					</div>
				</div>

			</div>


			<div class="card-body">
				
				<div class="row">
					<div class="col-sm-7 mx-auto">

						@if(count($date_visitors))

						<ul class="list-group">
			
							@foreach($date_visitors as $date_visitor)

							<a href="{{ route('companies.visitors.show', [$date_visitor->visitor->company, $date_visitor->visitor]) }}" 
								class="list-group-item list-group-item-action d-flex align-items-center">
								
								<span><b>{{ $date_visitor->full_name }}</b></span>

								<form action="{{ route('dates.visitors.destroy', [$date, $date_visitor]) }}" 
									method="post" 
									class="ml-auto">
									
									@csrf
									
									@method('DELETE')
									
									<button type="submit" 
										class="close text-danger" 
										aria-label="Close"
										title="Eliminar visitante"
										onclick='return confirm("¿Estás seguro que deseas remover a {{ $date_visitor->full_name }} de esta cita?")'>
									  	
									  	<span aria-hidden="true">&times;</span>

									</button>

								</form>
							</a>

							@endforeach

						</ul>

						@else
			
						<p class="m-0">No se han agregado visitantes a esta cita.</p>
	
						@endif

					</div>
				</div>

			</div>
		</div>

		@component('app.components.modal', [
			'id' => 'newVisitorModal',
			'title' => 'Agregar nuevo visitante',
		])
			
			@slot('body')
				
				<p>
					
					<b class="text-danger text-bold">Nota:</b>

					los visitantes que agregues aquí, 
					se asignarán a la compañía relacionada 
					con la cita ({{ $date->company_name }})

				</p>


				<form action="{{ route('set-new-visitor-from-date', $date) }}" 
					method="post" 
					class="mt-4" 
					id="newVisitorForm">

					@csrf

					<div class="form-group row">
						<div class="col-sm-7 offset-3">
							
							Campos requeridos marcardos con (<span class="text-danger">*</span>)

						</div>
					</div>
					
					<div class="form-group row">
						<label class="col-sm-3 col-form-label text-right">
							<span class="text-danger">*</span>
							Nombre(s):
						</label>
						<div class="col-sm-7">
							<input name="first_name" 
								type="text" 
								class="form-control @error('first_name') is-invalid @enderror" 
								required>
						</div>
					</div>


					<div class="form-group row">
						<label class="col-sm-3 col-form-label text-right">
							<span class="text-danger">*</span>
							Apellido(s):
						</label>
						<div class="col-sm-7">
							<input name="last_name" 
								type="text" 
								class="form-control @error('last_name') is-invalid @enderror"
								required>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-3 col-form-label text-right">E-mail:</label>
						<div class="col-sm-7">
							<input name="email" 
								type="email" 
								class="form-control @error('email') is-invalid @enderror">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-3 col-form-label text-right">Puesto laboral:</label>
						<div class="col-sm-7">
							<input name="jobtitle" 
								type="text" 
								class="form-control @error('jobtitle') is-invalid @enderror">
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-3 col-form-label text-right">
							<span class="text-danger">*</span>
							Género:
						</label>
						<div class="col-sm-4">
							<select name="gender_id" 
								class="custom-select @error('gender') is-invalid @enderror">
								<option disabled selected value="">Elige una opción</option>
								<option value="0">Masculino</option>
								<option value="1">Femenino</option>
							</select>
						</div>
					</div>

				</form>

			@endslot


			@slot('footer')
			
				<div class="w-100">
					<div class="row">
						<div class="col-sm-7 offset-3">
							<button type="submit" class="btn btn-primary" form="newVisitorForm">Guardar</button>
						</div>
					</div>
				</div>

			@endslot

		@endcomponent

	</div>

@stop