<div class="card shadow-sm">
	<div class="card-header d-flex align-items-center">
		<p class="lead m-0">{{ $title }}</p>
	</div>

	@if($containers->total())
		
		<table class="table table-striped m-0">
			
			<thead>
				<tr>
					<th nowrap>Contenedor</th>
					<th nowrap>Descripción</th>
					<th nowrap>Compañía</th>
					<th nowrap>Emisor</th>
					<th nowrap>No. candado</th>
					<th nowrap>No. contenedor</th>
					<th nowrap>No. pedimento</th>
				</tr>
			</thead>

			<tbody>
				
				@foreach($containers as $container)
	
				<tr>
					<td>
						<a href="{{ route('containers.show', $container) }}">
							{{ $container->name }}
						</a>
					</td>
					<td>{{ $container->description }}</td>
					<td>{{ optional($container->date)->company_name }}</td>
					<td>{{ $container->issuing_by }}</td>
					<td>{{ $container->padlock }}</td>
					<td>{{ $container->number }}</td>
					<td>{{ $container->petition }}</td>
				</tr>

				@endforeach

			</tbody>

		</table>

	@else


		<div class="card-body">
			<div class="row">
				<div class="col-sm-4 mx-auto text-center">
					<img src="{{ asset('img/undraw_logistics_x4dc.svg.svg') }}" class="svg_img w-50 mb-4" alt="">
					<p class="lead">No se han agregado contenedores aún.</p>
					<a href="#" class="btn btn-primary">
						<i class="fa fa-plus mr-2"></i> 
						Agrega uno
					</a>
				</div>
			</div>
		</div>


	@endif

	@if($containers->total() > $containers->perPage())

	<div class="card-footer pb-0">
		{{ $containers->links() }}
	</div>

	@endif
</div>