<div class="card shadow-sm">
	<div class="card-header d-flex align-items-center">
		<p class="lead m-0">{{ $title }}</p>
		<a href="{{ isset($route) ? $route : route('new-vehicle') }}" class="btn btn-primary ml-auto">
			<i class="fa fa-plus mr-2"></i>
			Nuevo
		</a>
	</div>
	
	@if($vehicles->total())

	<table class="table table-striped m-0">
		<thead>
			<tr>
				<th>Tipo</th>
				<th>Marca</th>
				<th>Modelo</th>
				<th>Año</th>
				<th>Placas</th>
				<th>Color</th>
				<th>Compañía</th>
				<th>Citas</th>
				<th class="text-right">Detalles</th>
			</tr>
		</thead>

		<tbody>
			@foreach($vehicles as $vehicle)
			<tr>
				<td>{{ $vehicle->type }}</td>
				<td>{{ $vehicle->brand }}</td>
				<td>{{ $vehicle->model }}</td>
				<td>{{ $vehicle->year }}</td>
				<td>{{ $vehicle->plates }}</td>
				<td>{{ $vehicle->color }}</td>
				<td>
					<a href="{{ route('companies.show', $vehicle->company) }}">
						{{ $vehicle->company_name }}
					</a>
				</td>
				<td>{{ $vehicle->dates_total }}</td>
				<td class="text-right">
					<a href="{{ route('companies.vehicles.show', [$vehicle->company, $vehicle]) }}">Ver más</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>

	@else

	<div class="card-body">
		
		<div class="row">
			<div class="col-sm-4 mx-auto text-center">
				<img src="{{ asset('img/undraw_city_driver_jh2h.svg') }}" class="svg_img w-50 mb-4" alt="">
				<p class="lead">No se han agregado vehículos aún.</p>
				<a href="{{ isset($route) ? $route : route('new-vehicle') }}" class="btn btn-primary">
					<i class="fa fa-plus mr-2"></i> 
					Agrega uno
				</a>
			</div>
		</div>

	</div>

	@endif
	
	@if($vehicles->total() > $vehicles->perPage())
	<div class="card-footer pb-0">
		{{ $vehicles->links() }}
	</div>
	@endif
</div>