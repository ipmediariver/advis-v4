<div class="mb-3">
	<a href="{{ $route }}">
		<i class="fa fa-angle-left mr-1 fa-sm"></i>
		{{$text}}
	</a>
</div>