@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">{{ $department->name }} | Nuevo empleado</p>
			</div>
			<div class="card-body">
				
				<form action="{{ route('departments.employees.store', $department) }}" method="post" id="newEmployeeForm">
		
					@include('app.employees.partials.form')

				</form>

			</div>
			<div class="card-footer">
				
				<div class="row">
					<div class="col-sm-6 offset-3">
						<button type="submit" class="btn btn-primary" form="newEmployeeForm">
							<i class="fa fa-check mr-2"></i>
							Guardar
						</button>
					</div>
				</div>

			</div>
		</div>
	</div>

@stop