@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Editar información</p>
			</div>
			<div class="card-body">
				
				@empty($department)

				<form action="{{ route('update-employee', $employee) }}" method="post" id="updateEmployeeForm">

				@else

				<form action="{{ route('departments.employees.update', [$department, $employee]) }}" method="post" id="updateEmployeeForm">

				@endif
		
					@method('PUT')

					@include('app.employees.partials.form')

				</form>

			</div>
			<div class="card-footer">
				
				<div class="row">
					<div class="col-sm-6 offset-3 d-flex align-items-center">
						<button type="submit" class="btn btn-primary" form="updateEmployeeForm">
							<i class="fa fa-check mr-2"></i>
							Actualizar
						</button>
						
						@empty($department)

						<form action="{{ route('destroy-employee', $employee) }}" 
								class="ml-2" 
								method="post">

						@else
					
						<form action="{{ route('departments.employees.destroy', [$employee->department, $employee]) }}" 
								class="ml-2" 
								method="post">

						@endif
							
							@csrf

							@method('DELETE')

							<button type="submit" 
								class="btn btn-outline-danger"
								onclick="return confirm('¿Estas seguro que deseas eliminar a este empleado?')">
								<i class="fa fa-trash-alt mr-2"></i>
								Eliminar
							</button>

						</form>
					</div>
				</div>

			</div>
		</div>
	</div>

@stop