@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Editar visitante</p>
			</div>
			<div class="card-body">
				
				<form action="{{ route('companies.visitors.update', [$company, $visitor]) }}" 
					method="post"
					id="editVisitorForm">
					
					@method('PATCH')

					@include('app.visitors.partials.form')

				</form>

			</div>
			<div class="card-footer">
				<div class="form-group row mb-0">
					<div class="col-sm-7 offset-3 d-flex align-items-center">
						<button type="submit" class="btn btn-primary" form="editVisitorForm">
							<i class="fa fa-check mr-2"></i>
							Actualizar
						</button>

						<form action="{{ route('companies.visitors.destroy', [$company, $visitor]) }}" method="post" class="ml-2">
							@csrf
							@method('DELETE')
							<button type="submit"
								class="btn btn-outline-danger" 
								onclick="return confirm('¿Estás seguro que deseas eliminar a {{$visitor->full_name }}?')">
								<i class="fa fa-trash-alt mr-2"></i>
								Eliminar
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>

	</div>

@stop