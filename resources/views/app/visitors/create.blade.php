@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">{{ $company->name}} | Nuevo visitante</p>
			</div>
			<div class="card-body">
				
				<form action="{{ route('companies.visitors.store', $company) }}" 
					method="post"
					id="newVisitorForm">

					@include('app.visitors.partials.form')

				</form>

			</div>
			<div class="card-footer">
				<div class="form-group row mb-0">
					<div class="col-sm-6 offset-3">
						<button type="submit" class="btn btn-primary" form="newVisitorForm">
							<i class="fa fa-check mr-2"></i>
							Guardar
						</button>
					</div>
				</div>
			</div>
		</div>

	</div>

@stop