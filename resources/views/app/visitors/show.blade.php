@extends('layouts.app')

@section('content')

	<div class="container-fluid">

		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Informacion de visitante</p>
				<a href="{{ route('companies.visitors.edit', [$company, $visitor]) }}" class="btn btn-primary ml-auto">
					<i class="fa fa-pencil-alt mr-2"></i>
					Editar
				</a>
			</div>
			<table class="table table-striped m-0">
				<tbody>
					<tr>
						<th width="20%">Nombre:</th>
						<td>{{ $visitor->full_name }}</td>
					</tr>
					<tr>
						<th>E-mail:</th>
						<td>{{ $visitor->email }}</td>
					</tr>
					<tr>
						<th>Puesto laboral:</th>
						<td>{{ $visitor->jobtitle }}</td>
					</tr>
					<tr>
						<th>Género:</th>
						<td>{{ $visitor->gender }}</td>
					</tr>
					<tr>
						<th>Tipo:</th>
						<td>{{ $visitor->type }}</td>
					</tr>
					<tr>
						<th>Compañía:</th>
						<td>
							<a href="{{ route('companies.show', $company) }}">{{ $company->name }}</a>
						</td>
					</tr>
					<tr>
						<th>Total de citas:</th>
						<td>{{ $visitor->dates_total }}</td>
					</tr>
				</tbody>
			</table>
		</div>

	</div>

@stop