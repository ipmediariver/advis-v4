@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Editar cita</p>
			</div>
			<div class="card-body">
					
		
				<form action="{{ route('dates.update', $date) }}" 
					method="post" 
					id="editDateForm"
					spellcheck="false">

					@csrf

					@method('PATCH')
					
					<div class="form-group row">
						<label class="col-sm-3 col-form-label text-right">Compañía:</label>
						<div class="col-sm-6">
							<input type="text" disabled class="form-control" value="{{ $date->company_name }}" important>
						</div>
					</div>


					<div class="form-group row">
						<label class="col-sm-3 col-form-label text-right">Asunto:</label>
						<div class="col-sm-6">
							
							<input id="text" 
								type="text" 
								class="form-control @error('subject') is-invalid @enderror" 
								value="{{ isset($date) ? $date->subject : old('subject') }}"
								name="subject" 
								required>
						
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-3 col-form-label text-right">
							Descripción
						</label>
						<div class="col-sm-6">
							<textarea class="form-control @error('description') is-invalid @enderror" 
								name="description" 
								id="description" 
								rows="5">{{ isset($date) ? $date->description : old('description') }}
								</textarea>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-3 col-form-label text-right">Fecha y hora</label>
						<div class="col-sm-6">

							<date-start date="{{ $date->start_at }}" />

						</div>
					</div>


					<div class="form-group row mb-0">
						<label class="col-sm-3 col-form-label text-right">Tiempo permitido:</label>
						<div class="col-sm-6">
							<select class="custom-select @error('vigency') is-invalid @enderror" name="vigency" id="vigency">
								<option disabled selected value="">Elige una opción</option>
								<option value="1" @isset($date) {{ $date->vigency == 1 ? 'selected' : '' }} @endisset>1 Hora</option>
								<option value="2" @isset($date) {{ $date->vigency == 2 ? 'selected' : '' }} @endisset>2 Horas</option>
								<option value="5" @isset($date) {{ $date->vigency == 5 ? 'selected' : '' }} @endisset>5 Horas</option>
								<option value="8" @isset($date) {{ $date->vigency == 8 ? 'selected' : '' }} @endisset>8 Horas</option>
								<option value="0" @isset($date) {{ $date->vigency == 0 ? 'selected' : '' }} @endisset>Todo el día</option>
							</select>
						</div>
					</div>	

				</form>

			</div>
			<div class="card-footer">
				<div class="row">
					<div class="col-sm-6 offset-3 d-flex align-items-center">
						<button class="btn btn-primary"
							form="editDateForm">
							<i class="fa fa-check mr-2"></i>
							Actualizar
						</button>

						<form action="{{ route('dates.destroy', $date) }}" method="post">
							
							@csrf
							
							@method('DELETE')

							<button type="submit" class="btn btn-outline-danger ml-2"
								onclick="return window.confirm('¿Estás seguro que deseas eliminar esta cita?')">
								<i class="fa fa-trash-alt mr-2"></i>
								Eliminar
							</button>

						</form>
					</div>
				</div>
			</div>
		</div>

	</div>

@stop