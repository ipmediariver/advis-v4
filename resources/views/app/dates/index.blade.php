@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		
		@component('app.components.dates-table', [
		
			'title' => 'Listado de citas',
			'dates' => $dates
		
		])

		@endcomponent

	</div>

@stop