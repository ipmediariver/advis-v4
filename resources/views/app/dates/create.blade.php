@extends('layouts.app')

@section('content')

	<div class="container-fluid">

		<new-date company="{{ request('company_id') }}" 
			department="{{ request('department_id') }}" 
			host="{{ request('host_id') }}"
			role="{{ auth()->user()->role }}"
			user_id="{{ auth()->user()->id }}"
			date_start_at="{{ request('date') }}" />

	</div>

@stop
