@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Editar contenedor</p>
			</div>
			<div class="card-body">
				
				<form action="{{ route('containers.update', $container) }}" method="post" id="updateContainerForm">
					
					@method('PATCH')

					
					@include('app.containers.partials.form')
						

				</form>

			</div>
			<div class="card-footer">
				
				<div class="row">
					<div class="col-sm-6 offset-3 d-flex">

						<button type="submit" class="btn btn-primary" form="updateContainerForm">
							<i class="fa fa-check mr-2"></i>
							Actualizar
						</button>

						<form action="{{ route('containers.destroy', $container) }}" method="post" class="ml-2">
							
							@csrf

							@method('DELETE')

							<button type="submit" 
								class="btn btn-outline-danger"
								onclick="return confirm('¿Estas seguro que deseas eliminar este contenedor?')">
								<i class="fa fa-trash-alt"></i>
								Eliminar
							</button>

						</form>

					</div>
				</div>

			</div>
		</div>

	</div>

@stop