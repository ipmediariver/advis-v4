@extends('layouts.app')

@section('content')
	

	<div class="container-fluid">
		
		@component('app.components.containers-table', [
			
			'title' => 'Lista de contenedores',
			'containers' => $containers
		
		])

		@endcomponent

	</div>


@stop