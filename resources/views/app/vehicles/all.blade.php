@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		@include('app.components.vehicles-table', [
			'title' 	=> 'Listado de vehículos',
			'vehicles' 	=> $vehicles
		])
	</div>

@stop