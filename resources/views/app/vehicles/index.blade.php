@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		@include('app.components.vehicles-table', [
			'title' 	=> $company->name . '| Listado de vehículos',
			'vehicles' 	=> $vehicles,
			'route' 	=> route('companies.vehicles.create', $company)
		])
	</div>

@stop