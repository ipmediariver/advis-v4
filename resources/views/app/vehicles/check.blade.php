@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">
					<i class="fa fa-car mr-2 text-muted"></i>
					Buscar vehículo
				</p>
			</div>
			<div class="card-body">
				
				<form action="{{ route('search-vehicle-plates') }}" id="searchVehicleForm" method="get">
				
					@csrf

					<div class="form-group row">
						<div class="col-sm-6 offset-0 offset-sm-3">
							<p class="mb-2">Introduce el número de placas del vehículo</p>
							<p class="text-muted m-0">(solo números y letras)</p>
						</div>
					</div>

					<div class="form-group row">
						<label class="col-sm-3 col-form-label text-left text-sm-right">No. placas</label>
						<div class="col-sm-6">
							<input type="text" 
								value="{{ old('plates') }}" 
								name="plates" 
								class="form-control" 
								required>
						</div>
					</div>

				</form>

			</div>
			<div class="card-footer">
				<div class="form-group row mb-0">
					<div class="col-sm-6 offset-0 offset-sm-3">
						<button type="submit" form="searchVehicleForm" class="btn btn-primary">
							<i class="fa fa-search mr-2"></i>
							Buscar
						</button>
					</div>
				</div>
			</div>
		</div>

	</div>

@stop