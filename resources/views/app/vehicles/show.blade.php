@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		<div class="card shadow-sm mb-4">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Información general</p>
				<a href="{{ route('companies.vehicles.edit', [$company, $vehicle]) }}" class="btn btn-primary ml-auto">
					<i class="fa fa-pencil-alt mr-2"></i>
					Editar
				</a>
			</div>
			<table class="table table-striped m-0">
				<tbody>
					<tr>
						<th width="20%">Tipo:</th>
						<td>{{ $vehicle->type }}</td>
					</tr>

					<tr>
						<th>Marca:</th>
						<td>{{ $vehicle->brand }}</td>
					</tr>

					<tr>
						<th>Modelo:</th>
						<td>{{ $vehicle->model }}</td>
					</tr>

					<tr>
						<th>Año:</th>
						<td>{{ $vehicle->year }}</td>
					</tr>

					<tr>
						<th>Placas:</th>
						<td>{{ $vehicle->plates }}</td>
					</tr>

					<tr>
						<th>Color:</th>
						<td>{{ $vehicle->color }}</td>
					</tr>

					<tr>
						<th>Compañía:</th>
						<td>
							<a href="{{ route('companies.show', $vehicle->company) }}">
								{{ $vehicle->company_name }}
							</a>
						</td>
					</tr>

					<tr>
						<th>Total de citas:</th>
						<td>
							{{ $vehicle->dates_total }}
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		@include('app.components.dates-table', [
			
			'dates' => $dates,
			'title' => '('. count($dates) .') Citas',
		
		])
	</div>

@stop