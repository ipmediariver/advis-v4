@csrf

<div class="form-group row">
	<div class="col-sm-6 offset-3">
		<p>Campos requeridos marcados con (<span class="text-danger">*</span>)</p>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right"><span class="text-danger">*</span>Nombre:</label>
	<div class="col-sm-6">
		<input type="text" 
			name="name" 
			class="form-control @error('name') is-invalid @enderror"
			value="{{ isset($meeting_room) ? $meeting_room->name : old('name') }}"
			required
			autofocus>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">Descripción:</label>
	<div class="col-sm-6">
		<textarea name="description" rows="3" class="form-control @error('description') is-invalid @enderror">{{ isset($meeting_room) ? $meeting_room->description : old('description') }}</textarea>
	</div>
</div>

<div class="form-group row">
	<label class="col-sm-3 col-form-label text-right">Ubicación:</label>
	<div class="col-sm-6">
		<input type="text" 
			name="location" 
			class="form-control @error('location') is-invalid @enderror"
			value="{{ isset($meeting_room) ? $meeting_room->location : old('location') }}">
	</div>
</div>