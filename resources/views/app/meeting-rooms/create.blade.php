@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Nueva sala de juntas</p>
			</div>
			<div class="card-body">
				
				<form action="{{ route('meeting-rooms.store') }}" method="post" id="newMeetingRoomForm">
					
					@include('app.meeting-rooms.partials.form')

				</form>

			</div>
			<div class="card-footer">
				
				<div class="row">
					<div class="col-sm-6 offset-3">
						<button type="submit" form="newMeetingRoomForm" class="btn btn-primary">
							<i class="fa fa-check mr-2"></i>
							Guardar
						</button>
					</div>
				</div>

			</div>
		</div>

	</div>

@stop