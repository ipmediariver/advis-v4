@extends('layouts.app')

@section('content')

	<div class="container-fluid">
		
		<div class="card shadow-sm">
			<div class="card-header d-flex align-items-center">
				<p class="lead m-0">Listado de salas de juntas</p>
				<a href="{{ route('meeting-rooms.create') }}" class="btn btn-primary ml-auto">
					<i class="fa fa-plus mr-2"></i>
					Nueva
				</a>
			</div>
			
			@if(count($meeting_rooms))
	
			<table class="table table-striped m-0">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Descripción</th>
						<th>Citas agendadas</th>
						<th class="text-right">Detalles</th>
					</tr>
				</thead>

				<tbody>
					@foreach($meeting_rooms as $meeting_room)
					<tr>
						<td>
							<a href="{{ route('meeting-rooms.show', $meeting_room) }}">{{ $meeting_room->name }}</a>
						</td>
						<td>{{ $meeting_room->description }}</td>
						<td>{{ $meeting_room->dates_total }}</td>
						<td class="text-right">
							<a href="{{ route('meeting-rooms.show', $meeting_room) }}">
								Ver más
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			@else

			<div class="card-body">
				<div class="row">
					<div class="col-sm-4 mx-auto text-center">
						<img src="{{ asset('img/undraw_business_plan_5i9d.svg') }}" class="svg_img w-50 mb-4" alt="">
						<p class="lead">No se han agregado salas de juntas aún.</p>
						<a href="{{ route('meeting-rooms.create') }}" class="btn btn-primary">
							<i class="fa fa-plus mr-2"></i> 
							Agrega una
						</a>
					</div>
				</div>
			</div>

			@endif
	
			@if($meeting_rooms->total() > $meeting_rooms->perPage())

			<div class="card-footer">
				{{ $meeting_rooms->links() }}
			</div>

			@endif
		</div>

	</div>

@stop