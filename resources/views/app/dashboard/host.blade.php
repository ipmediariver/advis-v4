@extends('layouts.app')

@section('content')
	
	<div class="container-fluid">
		
		@if(count($dates))
		
			@component('app.components.dates-table', [
		
				'dates' => $dates,
				'title' => $host->user->first_name . ', estas son tus citas para el día de hoy'

			])

			@endcomponent

		@else

			
			<div class="text-center">
				
				<p class="lead">Hola, {{ $host->full_name }}</p>

				<div class="row mb-3">
					<div class="col-sm-2 mx-auto">
						<img src="{{ asset('img/undraw_no_data_qbuo.svg') }}" class="img-fluid" alt="">
					</div>
				</div>
				
				<p>No tienes citas para el día de hoy. <a href="{{ route('dates.create') }}">¿Deseas agregar una?</a></p>

			</div>


		@endif

	</div>

@stop