@extends('layouts.app')

@section('content')
	
	

	<div class="container-fluid">

		<div class="card mb-4 shadow-sm">
			<div class="card-body">
				
				<div class="row">
					<div class="col-sm-4 mx-auto">
						<div class="form-row">
							<div class="col-sm-6 mb-3 mb-sm-0">
								<a href="{{ route('dates.create') }}" class="btn btn-block btn-outline-primary">
									<i class="fa fa-calendar-alt mr-2"></i> 
									Nueva cita
								</a>
							</div>
							<div class="col-sm-6">
								<a href="{{ route('check-vehicle') }}" class="btn btn-block btn-primary">
									<i class="fa fa-car mr-2"></i> 
									Recibir vehículo
								</a>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		
		<div class="card-group mb-4 d-none d-sm-flex">
			@foreach($indicators as $indicator)
			<div class="card shadow-sm">
				<div class="card-body">
					<p class="lead m-0">{{ $indicator['name'] }}</p>
					<h3 class="m-0">{{ $indicator['count'] }}</h3>
				</div>
			</div>
			@endforeach
		</div>
 
		@component('app.components.dates-table', [
		
			'title' => 'Citas para hoy',
			'dates' => $dates
		
		])

		@endcomponent

	</div>


@stop