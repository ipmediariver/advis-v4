<?php

use App\Company;
use App\Container;
use App\Date;
use App\DateContainer;
use App\DateVisitor;
use Illuminate\Database\Seeder;

class DatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(Date::class, 15)->create()->each(function ($date) {

            $company = Company::find(rand(1, 6));
            $visitor = $company->visitors->random();

            $date->update([
                'company_id'    => $company->id,
                'department_id' => rand(1, 5),
                'status_id'     => rand(0, 3),
            ]);

            factory(DateVisitor::class, 1)->create([

                'date_id'    => $date->id,
                'company_id' => $date->company_id,
                'visitor_id' => $visitor->id,

            ]);

            factory(Container::class, 1)->create()->each(function ($container) use ($date) {

                factory(DateContainer::class)->create([

                    'date_id'      => $date->id,
                    'container_id' => $container->id,

                ]);

            });

        });

    }
}
