<?php

use App\Vehicle;
use Illuminate\Database\Seeder;

class VehiclesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	factory(Vehicle::class, 50)->create()->each(function($vehicle){

    		$vehicle->update([
    			'company_id' => rand(1,5)
    		]);

    	});

    }
}
