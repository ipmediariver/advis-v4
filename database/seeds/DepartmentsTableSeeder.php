<?php

use App\Department;
use App\Employee;
use App\Host;
use App\User;
use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(Department::class, 5)->create()->each(function ($department) {

            factory(User::class, 2)->create([

                'role' => 'host',

            ])->each(function ($user) use ($department) {

                factory(Employee::class, 1)->create([

                    'department_id' => $department->id,
                    'first_name'    => $user->first_name,
                    'last_name'     => $user->last_name,
                    'email'         => $user->email,

                ])->each(function($employee) use ($department, $user) {

                    factory(Host::class, 1)->create([

                        'department_id' => $department->id,
                        'user_id'       => $user->id,
                        'employee_id'   => $employee->id

                    ]);

                });

            });

        });

    }
}
