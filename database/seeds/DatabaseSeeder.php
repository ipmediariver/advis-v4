<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(MeetingRoomsTableSeeder::class);
        $this->call(DepartmentsTableSeeder::class);
        $this->call(EmployeesTableSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(VehiclesTableSeeder::class);
        $this->call(VisitorsTableSeeder::class);
        $this->call(DatesTableSeeder::class);
    }
}
