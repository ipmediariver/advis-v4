<?php

use App\Visitor;
use Illuminate\Database\Seeder;

class VisitorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

    	factory(Visitor::class, 30)->create()->each(function($visitor){

    		$visitor->update([

	    		'company_id' => rand(1,6)

	    	]);

    	});


    }
}
