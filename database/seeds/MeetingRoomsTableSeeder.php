<?php

use App\MeetingRoom;
use Illuminate\Database\Seeder;

class MeetingRoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	factory(MeetingRoom::class, 5)->create();

    }
}
