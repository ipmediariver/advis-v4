<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDateVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('date_visitors', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('date_id');
            $table->integer('visitor_id');
            $table->integer('company_id');
            $table->integer('status_id')->default(0);
            $table->text('card_id')->nullable();
            $table->text('face_id')->nullable();
            $table->text('date_visitor_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('date_visitors');
    }
}
