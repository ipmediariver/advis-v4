<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Vehicle;
use Faker\Generator as Faker;

$factory->define(Vehicle::class, function (Faker $faker) {
    
    return [
    	'year' 		 => rand(2000,2020),
        'brand'      => $faker->randomElement($array = array ('Nissan','Honda','Chevrolet')),
        'model'      => $faker->randomElement($array = array ('Versa','Civic','Aveo')),
        'color'      => $faker->randomElement($array = array ('blanco','azul','negro')),
        'type_id'    => rand(0,15),
        'plates'     => rand(50000,70000),
        'company_id' => 1,
    ];

});
