<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\EmployeeHistory;
use Faker\Generator as Faker;

$factory->define(EmployeeHistory::class, function (Faker $faker) {
    return [
        'employee_id' => 1,
        'description' => $faker->paragraph,
    ];
});
