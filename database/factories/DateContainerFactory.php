<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\DateContainer;
use Faker\Generator as Faker;

$factory->define(DateContainer::class, function (Faker $faker) {
    return [

        'date_id'      => 1,
        'container_id' => 1,

    ];
});
