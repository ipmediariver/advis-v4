<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Host;
use Faker\Generator as Faker;

$factory->define(Host::class, function (Faker $faker) {
    return [
        'department_id' => 1,
        'user_id'       => 1,
    ];
});
