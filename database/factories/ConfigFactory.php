<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Config;
use Faker\Generator as Faker;

$factory->define(Config::class, function (Faker $faker) {
    return [
        'name'    => 'ADVIS - Administrador De Visitas',
        'address' => '',
        'phone'   => '',
        'rfc'     => '',
        'logo'    => '',
    ];
});
