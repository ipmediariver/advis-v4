<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Visitor;
use Faker\Generator as Faker;

$factory->define(Visitor::class, function (Faker $faker) {
    return [
        
    	'first_name' => $faker->firstName,
    	'last_name' => $faker->lastName,
    	'email' => $faker->email,
    	'jobtitle' => $faker->jobTitle,
    	'type_id' => rand(0,3),
    	'company_id' => 1,
    	'gender_id' => rand(0,1),

    ];
});
