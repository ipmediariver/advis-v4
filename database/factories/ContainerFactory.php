<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Container;
use Faker\Generator as Faker;

$factory->define(Container::class, function (Faker $faker) {
    return [
        
    	'name' => $faker->word,
    	'description' => $faker->sentence,
    	'issuing_by' => 'Institución Emisora S.A de C.V.',
    	'padlock' => rand(1000,9000),
    	'number' => rand(1000,9000),
    	'petition' => rand(1000,9000),
    	'comments' => '',
    	'field_1' => '',
    	'field_2' => '',
    	'field_3' => '',
    	'field_4' => '',
    	'field_5' => '',
    	'field_6' => '',
    	'field_7' => '',
    	'field_8' => '',
    	'field_9' => '',

    ];
});
